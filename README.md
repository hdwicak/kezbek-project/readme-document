# KEZBEK PROJECT 

## Architecture 
![img](./md_pics/kezbek-architecture.png)

Kezbek system is developed based on microservice architecture, using SAGA pattern for managing transactions across services. 
Tech stack used in this project: 
1. NestJS for building all service in kezbek back-end system
2. MySQL for database-engine 
3. Kafka for message-bus transport 

## Repository directory
![img](./md_pics/kezbek-repo.png) 
Kezbek Repository hosted on gitlab (link as stated below). 
Please refer to README Document directory for information about this project 



Microservices in Kezbek Back-End system consist of : 
1. [API Gateway](https://gitlab.com/hdwicak/kezbek-project/apigw), handling authentication for request sent from Kezbek's partner, and to make callback API call to partner 
2. [Service-Promo](https://gitlab.com/hdwicak/kezbek-project/service-promo), handling calculation for instant cashback. 
3. [Service-Loyalty](https://gitlab.com/hdwicak/kezbek-project/service-loyalty), handling calculation for loyalty cashback. 
4. [Service-Transaction](https://gitlab.com/hdwicak/kezbek-project/service-transaction), handling payment and mail service. 

## Running Kezbek App through docker compose : 
1. clone repo [kezbek-kafka](https://gitlab.com/hdwicak/kezbek-project/kezbek-kafka) 
2. change directory into kezbek-kafka 
3. run command : `docker compose up` 
4. ensure that all container are running, if some container stop just klik the start button on the docker-desktop application 

## Notes : 
1. Service API-Gateway expose API endpoint to be called by external partner, OpenAPI document can be viewed [here](http://localhost:3000/document) 
2. To make api call through API-Gateway :
    ```
    curl --location --request POST 'localhost:3000/transaction' \
    --header 'X-API-KEY: b4500a39fb7132447d18778784ce4fd0' \
    --header 'Content-Type: application/json' \
    --data-raw '{

    "trx_date" : "2023-01-08",
    "partner_code" : "1XCX1",
    "partner_key" : "aabbccddeeff",
    "promo_code": "DEC001",
    "customer_email": "jdoe@gmail.com",
    "payment_wallet": "BKL",
    "order_id" : "ORXB777N",
    "purchase_quantity": 2,
    "purchase_amount": 1000000,
    "description": "lorem ipsum"

    }
    ``` 

3. Kafdrop is GUI tools to inspect kafka, can be accessed via browser through this [link](http://localhost:19000/) 
4. Database MySql can be accessed by sql client software through this configuration : 
    - host : localhost
    - port : 3307
    - username : root
    - p assword : root







